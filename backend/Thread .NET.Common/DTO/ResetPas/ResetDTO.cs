﻿using System.ComponentModel.DataAnnotations;

namespace Thread_.NET.Common.DTO.ResetPas
{
    public class ResetDTO
    {
        [EmailAddress]
        public string Email { get; set; }
    }
}
