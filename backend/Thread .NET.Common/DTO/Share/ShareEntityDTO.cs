﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Thread_.NET.Common.DTO.Share
{
    public sealed class ShareEntityDTO
    {
        [EmailAddress]
        public string Email { get; set; }
        public int EntityId { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
    }
}
