﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Sender;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.ResetPas;
using Thread_.NET.Common.Security;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class ResetPasswordService : BaseService
    {
        private readonly EmailSender _emailSender;
        char[] chars = new char[] { '[', ']', '-', '/', '{', '}', '(', ')', '*', '+', '?', '^', '$', '%', ' '};
        public ResetPasswordService(EmailSender emailSender, ThreadContext context, IMapper mapper) 
            : base(context, mapper)
        {
            _emailSender = emailSender;
        }

        public async Task Reset(ResetDTO reset)
        {
            string baseUrl = "https://localhost:44344";
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == reset.Email);
            if(user == null)
            {
                throw new NotFoundException(reset.Email);
            }
            string pass = user.Password;
            foreach (char c in chars)
            {
                pass = pass.Replace($"{c}", "");
            }
            await _emailSender.SendEmailAsync(reset.Email, "Reset Password", $"<h3>Click this <a href='{baseUrl}/api/resetpassword/confirm/?userId={user.Id}&key={pass}'>link</a> for reset you password </h3>");        
        }
        public async Task<string> Confirm(int userId, string key)
        {
           var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
            string pass = user.Password;
            foreach (char c in chars)
            {
                pass = pass.Replace($"{c}", "");
            }
            if (pass == key)
            {
                string newPassword = GenerateRandomPassword(8);

                var salt = SecurityHelper.GetRandomBytes();

                var newSalt = Convert.ToBase64String(salt);
                var hashPassword = SecurityHelper.HashPassword(newPassword, salt);

                user.Salt = newSalt;
                user.Password = hashPassword;
                await _context.SaveChangesAsync();
                await _emailSender.SendEmailAsync(user.Email, "New Password", $"<h3>New Password - {newPassword}</h3>");
                return newPassword;
            }
            throw new NotFoundException("key");
        }
        private string GenerateRandomPassword(int passwordLength)
        {
            const string SetChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = SetChars[rd.Next(0, SetChars.Length - 1)];
            }

            return new string(chars);
        }

    }
}
