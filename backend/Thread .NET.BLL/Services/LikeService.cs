﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Sender;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly EmailSender _emailSender;
        public LikeService(EmailSender emailSender, ThreadContext context, IMapper mapper) : base(context, mapper) 
        {
            _emailSender = emailSender;
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (likes.Any())
            {
                // If there was a like - true, and dislike do change to - true or vice versa
                var like = likes.Where(x => x.IsLike != reaction.IsLike || x.IsDislike != reaction.IsDislike).FirstOrDefault();

                if(like != null)
                {
                    like.IsLike = reaction.IsLike;
                    like.IsDislike = reaction.IsDislike;
                }
                // When like or dislike was canceled
                else
                {
                    _context.PostReactions.RemoveRange(likes);
                }
                await _context.SaveChangesAsync();
                return;
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                IsDislike = reaction.IsDislike,
                UserId = reaction.UserId
            });
            await _context.SaveChangesAsync();

            if (reaction.IsLike)
            {
                var post = await _context.Posts
                                .Include(post => post.Author)
                                .FirstAsync(post => post.Id == reaction.EntityId);

                var user = await _context.Users.FindAsync(reaction.UserId);
                var email = post.Author.Email;

                await _emailSender.SendEmailAsync(email, "You have new like!", $"<h3>{user.UserName} liked your post.</h3>");
            }
        }
        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (likes.Any())
            {
                var like = likes.Where(x => x.IsLike != reaction.IsLike || x.IsDislike != reaction.IsDislike).FirstOrDefault();

                if (like != null)
                {
                    like.IsLike = reaction.IsLike;
                    like.IsDislike = reaction.IsDislike;
                }
                else
                {
                    _context.CommentReactions.RemoveRange(likes);
                }
                await _context.SaveChangesAsync();
                return;
            }

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                IsDislike = reaction.IsDislike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}
