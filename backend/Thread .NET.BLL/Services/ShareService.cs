﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Share;
using Thread_.NET.DAL.Context;
using Thread_.NET.BLL.Sender;

namespace Thread_.NET.BLL.Services
{
    public sealed class ShareService : BaseService
    {
        private readonly EmailSender _emailSender;
        public ShareService(EmailSender emailSender, ThreadContext context, IMapper mapper) : base(context, mapper)
        {
            _emailSender = emailSender;
        }
        public async Task SharePost(ShareEntityDTO share)
        {
            var post = await _context.Posts
                .Include(post=> post.Preview)
                .FirstAsync(post => post.Id == share.EntityId);

            var user = await _context.Users.FirstAsync(user => user.Id == share.UserId);

            if(post.Preview != null)
            {
                await _emailSender.SendEmailAsync(share.Email, $"{user.UserName} shared a post",
                $"<div><img alt='Photo' src='{post.Preview.URL}'><p>{post.Body}</p></div>");
            }
            else
            {
                await _emailSender.SendEmailAsync(share.Email, $"{user.UserName} shared a post",
                $"<p>{post.Body}</p>");
            }
           
        }
    }
}
