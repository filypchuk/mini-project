﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) 
        {        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }
        public async Task<CommentDTO> UpdateComment(CommentUpdateDTO commentDto)
        {
            var commentEntity = await _context.Comments.FindAsync(commentDto.Id);
            if (commentEntity == null)
            {
                throw new NotFoundException("comment", commentDto.Id);
            }

            if (commentEntity.AuthorId != commentDto.AuthorId)
            {
                throw new ForbiddenExeption("update", "comment", commentDto.Id);
            }

            commentEntity.Body = commentDto.Body;
            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();

            var updatedComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(comment => comment.Reactions)
                    .ThenInclude(reactions => reactions.User)
               .FirstAsync(comment => comment.Id == commentEntity.Id);

            var updatedCommentDto = _mapper.Map<CommentDTO>(updatedComment);
            return updatedCommentDto;

        }
        public async Task<CommentDTO> DeleteComment(int userId, int commentId)
        {
            var comment = await _context.Comments.
                Include(comment => comment.Author)
                .Include(comment => comment.Reactions)
                .FirstAsync(comment => comment.Id == commentId);
            if (comment == null)
            {
                throw new NotFoundException("comment", commentId);
            }

            if (comment.AuthorId != userId)
            {
                throw new ForbiddenExeption("delete", "comment", commentId);
            }
            var dletedCommentDto = _mapper.Map<CommentDTO>(comment);
            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
            return dletedCommentDto;
        }
    }
}
