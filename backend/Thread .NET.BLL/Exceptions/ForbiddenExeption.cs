﻿using System;


namespace Thread_.NET.BLL.Exceptions
{
    public sealed class ForbiddenExeption : Exception
    {
        public ForbiddenExeption(string operation, string entity, int id)
            : base($"You do not have access to {operation} entity {entity} with id ({id})")
        {
        }

        public ForbiddenExeption() : base("You do not have access") { }
    }
}
