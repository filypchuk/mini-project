﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;
        private readonly LikeService _likeService;

        public CommentsController(CommentService commentService, LikeService likeService)
        {
            _commentService = commentService;
            _likeService = likeService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreateComment([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }
        [HttpPut]
        [AllowAnonymous]
        public async Task<ActionResult<CommentDTO>> UpdateComment([FromBody] CommentUpdateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _commentService.UpdateComment(dto));
        }
        [HttpDelete("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<CommentDTO>> DeleteComment(int id)
        {
            var authorId = this.GetUserIdFromToken();
            return Ok(await _commentService.DeleteComment(authorId, id));
        }
        [HttpPost("like")]
        [AllowAnonymous]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikeComment(reaction);
            return Ok();
        }
    }
}