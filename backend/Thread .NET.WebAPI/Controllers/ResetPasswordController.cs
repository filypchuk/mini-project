﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.ResetPas;

namespace Thread_.NET.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResetPasswordController : ControllerBase
    {
        private readonly ResetPasswordService _resetPasswordService;
        public ResetPasswordController(ResetPasswordService resetPasswordService)
        {
            _resetPasswordService = resetPasswordService;

        }
        [HttpPost]
        public async Task<IActionResult> Reset(ResetDTO resetDTO)
        {
            await _resetPasswordService.Reset(resetDTO);
            return Ok();
        }
        [HttpGet("confirm")]
        public async Task<IActionResult> Confirm(int userId, string key)
        {
            return Ok(await _resetPasswordService.Confirm(userId, key));
        }
    }
}
