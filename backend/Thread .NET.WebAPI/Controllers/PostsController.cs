﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.Share;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly ShareService _shareService;
        public PostsController(PostService postService, LikeService likeService, ShareService shareService)
        {
            _postService = postService;
            _likeService = likeService;
            _shareService = shareService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }
        [HttpPut]
        public async Task<ActionResult<PostDTO>> UpdatePost([FromBody] PostUpdateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.UpdatePost(dto));
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<PostDTO>> DeletePost(int id)
        {
            var authorId = this.GetUserIdFromToken();
            return Ok(await _postService.DeletePost(authorId, id));
        }
        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikePost(reaction);
            return Ok();
        }
        [HttpPost("share")]
        public async Task<IActionResult> SharePost(ShareEntityDTO shareDto)
        {
            shareDto.UserId =  this.GetUserIdFromToken();
            await _shareService.SharePost(shareDto);
            return Ok();
        }
    }
}