import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';
import { Comment } from '../models/comment/comment';
import { CommentService } from './comment.service';
@Injectable({ providedIn: 'root' })
export class LikeService {

    public constructor(
        private authService: AuthenticationService,
        private postService: PostService,
        private commentService: CommentService
    ) { }

    public likePost(post: Post, currentUser: User, like: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: like,
            isDislike: !like,
            userId: currentUser.id
        };
        // update current array instantly
        innerPost.reactions = this.changeReaction(post.reactions, currentUser, like);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = this.changeReaction(post.reactions, currentUser, like);
                return of(innerPost);
            })
        );
    }
    public likeComment(comment: Comment, currentUser: User, like: boolean) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: like,
            isDislike: !like,
            userId: currentUser.id
        };
        innerComment.reactions = this.changeReaction(comment.reactions, currentUser, like);

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                innerComment.reactions = this.changeReaction(comment.reactions, currentUser, like);
                return of(innerComment);
            })
        );
    }
    public changeReaction(reactions: Reaction[], currentUser: User, like: boolean): Reaction[] {

        let reaction = reactions.find((x) => x.user.id === currentUser.id);
        // when new reaction
        if (reaction === null || reaction === undefined) {
            return reactions.concat({ isLike: like, isDislike: !like, user: currentUser });
        }
        else {
            // If there was a like - true, and dislike do change to - true or vice versa
            if (reaction.isLike !== like) {
                reactions = reactions.filter((x) => x.user.id !== currentUser.id);
                return reactions.concat({ isLike: like, isDislike: !like, user: currentUser });
            }
            // When like or dislike was canceled
            else {
                return reactions.filter((x) => x.user.id !== currentUser.id);
            }

        }
    }
}
