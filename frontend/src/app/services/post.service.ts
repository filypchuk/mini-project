import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { UpdatePost } from '../models/post/update-post';
import { ShareEntity } from '../models/share/share-entyti';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }
    public updatePost(post: UpdatePost) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}`, post);
    }
    public deletePost(postId: number) {
        return this.httpService.deleteFullRequest<Post>(`${this.routePrefix}/${postId}`);
    }
    public sharePost(share: ShareEntity) {
        return this.httpService.postClearRequest(`${this.routePrefix}/share`, share);
    }
}
