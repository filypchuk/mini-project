export interface ShareEntity {
    email: string;
    entityId: number;
    userId: number;
}