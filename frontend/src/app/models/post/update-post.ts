export interface UpdatePost {
    id: number;
    authorId: number;
    previewImage: string;
    body: string;
}