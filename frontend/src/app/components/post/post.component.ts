


import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { Reaction } from 'src/app/models/reactions/reaction';
import { PostService } from 'src/app/services/post.service';
import { UpdatePost } from 'src/app/models/post/update-post';
import { NotificationService } from 'src/app/services/notification.service';
import { FormControl, Validators } from '@angular/forms';
import { ShareEntity } from 'src/app/models/share/share-entyti';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;
    public postLikeReaction: Reaction[];
    public postDislikeReaction: Reaction[];
    public showComments = false;
    public editPost = false;
    public newComment = {} as NewComment;
    public showShare = false;

    private unsubscribe$ = new Subject<void>();
    email = new FormControl('', [Validators.required, Validators.email]);

    public constructor(
        private toastr: NotificationService,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService
    ) {

    }
    public ngOnInit() {
        this.countingReactions();
    }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    getErrorMessage() {
        if (this.email.hasError('required')) {
            return 'You must enter a value';
        }
        return this.email.hasError('email') ? 'Not a valid email' : '';
    }

    deletePost() {
        this.postService.deletePost(this.post.id).subscribe(
            (resp) => {
                this.post = resp.body;
                this.toastr.showWarning(`Post ${resp.body.id} was deleted.`, "deleted");
            },
            (error) => {
                this.toastr.showError(error, "error");
            });
    }
    updatePost() {
        this.editPost = false;
        let upPost: UpdatePost = {
            id: this.post.id,
            authorId: this.post.author.id,
            body: this.post.body,
            previewImage: this.post.previewImage
        };
        this.postService.updatePost(upPost).subscribe(
            (resp) => {
                this.post = resp.body;
                this.toastr.showInfo(`Post ${resp.body.id} was updated`, "updated");
            },
            (error) => {
                this.toastr.showError(error, "error");
            });
    }
    sharePost(value: string) {
        this.showShare = false;
        let share: ShareEntity = {
            email: value,
            entityId: this.post.id,
            userId: this.currentUser.id
        };
        this.postService.sharePost(share).subscribe();
    }
    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }
    public countingReactions() {
        this.postLikeReaction = this.post.reactions.filter(x => x.isLike === true);
        this.postDislikeReaction = this.post.reactions.filter(x => x.isDislike === true);
    }
    public likePost(like: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, like)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));
            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, like)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));

        this.countingReactions();
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                        this.toastr.showSuccess(`Comment ${resp.body.id} was created.`, "created");
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
    deletedCommentEvent(comment: Comment) {
        this.post.comments.splice(this.post.comments.findIndex(i => i.id === comment.id), 1);
    }
}
