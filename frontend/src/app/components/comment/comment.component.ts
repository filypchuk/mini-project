import { Component, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { Reaction } from 'src/app/models/reactions/reaction';
import { Subject } from 'rxjs';
import { LikeService } from 'src/app/services/like.service';
import { CommentService } from 'src/app/services/comment.service';
import { UpdateComment } from 'src/app/models/comment/update-comment';
import { takeUntil } from 'rxjs/operators';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public postId: number;
    @Output() public deletedComment = new EventEmitter<Comment>();
    public commentLikeReaction: Reaction[];
    public commentDislikeReaction: Reaction[];
    public editComment = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private toastr: NotificationService,
        private likeService: LikeService,
        private commentService: CommentService
    ) { }
    public ngOnInit() {
        this.countingReactions();
    }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    deleteComment() {
        this.commentService.deleteComment(this.comment.id).subscribe(
            (resp) => {
                this.toastr.showWarning(`Comment ${resp.body.id} was deleted`, "deleted");
            }, (error) => {
                this.toastr.showError(error, "error");

            }
        );
        this.deletedComment.emit(this.comment);
    }
    updateComment() {
        this.editComment = false;
        let upComment: UpdateComment = {
            id: this.comment.id,
            postId: this.postId,
            authorId: this.comment.author.id,
            body: this.comment.body
        };
        this.commentService.updateComment(upComment).subscribe(
            (resp) => {
                this.comment = resp.body;
                this.toastr.showInfo(`Comment ${resp.body.id} was updated`, "updated");
            },
            (error) => {
                this.toastr.showError(error, "error");
            });
    }
    public countingReactions() {
        this.commentLikeReaction = this.comment.reactions.filter(x => x.isLike === true);
        this.commentDislikeReaction = this.comment.reactions.filter(x => x.isDislike === true);
    }
    public likeComment(like: boolean) {
        this.likeService
            .likeComment(this.comment, this.currentUser, like)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));

        this.countingReactions();
    }
}
