import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { FormControl, Validators } from '@angular/forms';
import { ResetPassword } from 'src/app/models/auth/reset-password';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
    templateUrl: './auth-dialog.component.html',
    styleUrls: ['./auth-dialog.component.sass']
})
export class AuthDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public userName: string;
    public password: string;
    public avatar: string;
    public email: string;

    public hidePass = true;
    public title: string;
    private unsubscribe$ = new Subject<void>();

    public showResetPassword = false;
    sendToEmail = new FormControl('', [Validators.required, Validators.email]);

    constructor(
        private toastr: NotificationService,
        private dialogRef: MatDialogRef<AuthDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private authService: AuthenticationService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnInit() {
        this.avatar = 'https://cdn2.vectorstock.com/i/thumb-large/66/56/man-face-emotive-icon-in-flat-style-vector-13466656.jpg';
        this.title = this.data.dialogType === DialogType.SignIn ? 'Sign In' : 'Sign Up';
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    getErrorMessage() {
        if (this.sendToEmail.hasError('required')) {
            return 'You must enter a value';
        }
        return this.sendToEmail.hasError('email') ? 'Not a valid email' : '';
    }
    resetPassword(value: string) {
        this.showResetPassword = false;
        let reset: ResetPassword = {
            email: value,
        };
        this.authService.resetPassword(reset)
            .subscribe(() => { },
                (error) => this.snackBarService.showErrorMessage(error));
        this.toastr.showSuccess(value, "send");
    }
    public close() {
        this.dialogRef.close(false);
    }

    public signIn() {
        this.authService
            .login({ email: this.email, password: this.password })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }

    public signUp() {
        this.authService
            .register({ userName: this.userName, password: this.password, email: this.email, avatar: this.avatar })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }
}
